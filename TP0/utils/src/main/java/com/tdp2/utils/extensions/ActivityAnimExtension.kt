package com.tdp2.utils.extensions

import android.app.Activity
import com.tdp2.utils.R

//Call these methods after call startActivity()

fun Activity.pushAnimation() {
    overridePendingTransition(R.anim.anim_pull_in_right, R.anim.anim_push_out_left)
}

fun Activity.popAnimation() {
    overridePendingTransition(R.anim.anim_pull_in_left, R.anim.anim_push_out_right)
}

fun Activity.fadeInAnimation() {
    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
}

fun Activity.fadeOutAnimation() {
    overridePendingTransition(R.anim.fade_out, R.anim.fade_in)
}

fun Activity.openModalAnimation() {
    overridePendingTransition(R.anim.anim_pull_in_bottom, R.anim.fade_out)
}

fun Activity.closeModalAnimation() {
    overridePendingTransition(R.anim.fade_in, R.anim.anim_push_out_bottom)
}


fun Activity.fadeInOutAnimation() {
    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
}
