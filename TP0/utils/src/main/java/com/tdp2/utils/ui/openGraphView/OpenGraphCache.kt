package com.tdp2.utils.ui.openGraphView

import android.graphics.Bitmap
import android.text.TextUtils
import android.util.LruCache
import com.tdp2.utils.ui.openGraphView.model.OGData

object OpenGraphCache {

    private val mOGDataCache: LruCache<String, OGData>
    private val mImageCache: LruCache<String, Bitmap>

    init {
        val max = (Runtime.getRuntime().maxMemory() / 1024).toInt()
        val cacheSize = max / 8
        mOGDataCache = LruCache(cacheSize)
        mImageCache = LruCache(cacheSize)
    }

    fun add(
        url: String,
        ogData: OGData
    ) {
        if (get(url) == null) {
            mOGDataCache.put(url, ogData)
        }
    }

    fun get(url: String): OGData? {
        return if (TextUtils.isEmpty(url)) {
            null
        } else mOGDataCache[url]
    }

    fun addImage(url: String, bitmap: Bitmap?) {
        if (TextUtils.isEmpty(url) || bitmap == null) {
            return
        }
        if (getImage(url) == null) {
            mImageCache.put(url, bitmap)
        }
    }

    fun getImage(url: String): Bitmap? {
        return if (TextUtils.isEmpty(url)) {
            null
        } else mImageCache[url]
    }
}