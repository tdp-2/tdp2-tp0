package com.tdp2.utils.ui.openGraphView

abstract class OnLoadListener {

    abstract fun onLoadStart()

    abstract fun onLoadFinish()

    abstract fun onLoadError(e: Throwable?)
}