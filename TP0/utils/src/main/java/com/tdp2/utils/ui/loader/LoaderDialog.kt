package com.tdp2.utils.ui.loader

import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.annotation.DrawableRes
import com.tdp2.utils.R
import kotlinx.android.synthetic.main.dialog_loader.*

class LoaderDialog private constructor(context: Context) : Dialog(context, R.style.DialogTheme) {

    private var param: LoaderParam? = null
    private var animation: ObjectAnimator? = null

    init {
        val windowLayoutParams = WindowManager.LayoutParams()
        windowLayoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        windowLayoutParams.dimAmount = 0.3f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_loader)

        param?.iconDrawable?.apply {
            iconImageView.setImageDrawable(context.getDrawable(this))
        }
    }

    override fun show() {
        super.show()
        startAnimation()
    }

    override fun dismiss() {
        hideAnimation()
        super.dismiss()
    }

    private fun startAnimation() {
        animation = ObjectAnimator.ofFloat(
            iconImageView,
            "rotationY", 0.0f, 360f
        ).apply {
            duration = 600
            repeatCount = ObjectAnimator.INFINITE
            interpolator = AccelerateDecelerateInterpolator()
            start()
        }

    }

    private fun hideAnimation() {
        animation?.cancel()
    }

    private class LoaderParam {
        var iconDrawable: Int? = null
    }

    @Suppress("unused")
    class Builder(private var context: Context) {
        private var param = LoaderParam()

        fun setIcon(@DrawableRes id: Int): Builder =
            apply { param.iconDrawable = id }

        fun create(): LoaderDialog {
            val dialog = LoaderDialog(context)
            dialog.param = param
            return dialog
        }
    }
}