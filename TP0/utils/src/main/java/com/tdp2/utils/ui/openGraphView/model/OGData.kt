package com.tdp2.utils.ui.openGraphView.model

data class OGData(
    var title: String? = null,
    var description: String? = null,
    var image: String? = null,
    var url: String? = null
)