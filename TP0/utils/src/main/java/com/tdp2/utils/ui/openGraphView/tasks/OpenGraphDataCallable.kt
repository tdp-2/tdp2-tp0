package com.tdp2.utils.ui.openGraphView.tasks

import com.tdp2.utils.ui.openGraphView.Parser
import com.tdp2.utils.ui.openGraphView.model.OGData
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Callable

class OpenGraphDataCallable(private val url: String, private val parser: Parser) : Callable<OGData> {

    override fun call(): OGData? {
        var inputStream: InputStream? = null
        var data: OGData? = null
        try {
            inputStream = downloadUrl(url)
            data = parser.parse(inputStream)
        } catch (e: IOException) {
            return null
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (e: IOException) { //nop
                }
            }
        }
        return data
    }

    @Throws(IOException::class)
    private fun downloadUrl(urlString: String): InputStream {
        val url = URL(urlString)
        val conn = url.openConnection() as HttpURLConnection
        conn.readTimeout = 10000
        conn.connectTimeout = 15000
        conn.requestMethod = "GET"
        conn.setRequestProperty("User-Agent", "")
        conn.doInput = true
        conn.connect()
        return conn.inputStream
    }
}