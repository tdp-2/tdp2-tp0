package com.tdp2.utils.general

import androidx.lifecycle.LiveData

/**
 * A LiveData class that has `null` value.
 */
@Suppress("unused")
class AbsentLiveData<T : Any?> private constructor() : LiveData<T>() {
    init {
        // use post instead of set since this can be created on any thread
        postValue(null)
    }

    companion object {
        fun <T> create(): LiveData<T> {
            return AbsentLiveData()
        }
    }
}