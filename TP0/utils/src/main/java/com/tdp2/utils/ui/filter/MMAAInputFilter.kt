package com.tdp2.utils.ui.filter

import android.text.InputFilter
import android.text.Spanned

@Suppress("unused")
class MMAAInputFilter : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        if (source.toString().toIntOrNull() == null)
            return ""

        if ((dest != null) && (dest.toString().replace("/", "").length > 24)) {
            return null
        }

        return if (source.length == 1 && (dstart == 2)) {
            "/$source"
        } else {
            null
        }
    }
}