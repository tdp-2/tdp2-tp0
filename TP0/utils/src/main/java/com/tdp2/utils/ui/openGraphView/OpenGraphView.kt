package com.tdp2.utils.ui.openGraphView

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.net.Uri
import android.text.Spanned
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.tdp2.utils.R
import com.tdp2.utils.ui.openGraphView.model.OGData
import com.tdp2.utils.ui.openGraphView.tasks.*

class OpenGraphView : RelativeLayout {

    enum class IMAGE_POSITION {
        LEFT, RIGHT
    }

    private var mRootContainer: View
    @ColorInt
    private var mStrokeColor = -1
    @ColorInt
    private var mBgColor = -1
    private var mViewWidth = 0
    private var mViewHeight = 0
    private var mStrokeWidth = 0f
    private var mCornerRadius = 0f
    private var mSeparate = false
    private lateinit var mSeparator: View
    private lateinit var mRoundableImageView: RoundableImageView
    private lateinit var mFavicon: ImageView
    private var mUri: Uri? = null
    private var mUrl: String? = null
    private var mOnLoadListener: OnLoadListener? = null
    private val mFillRect = RectF()
    private val mStrokeRect = RectF()
    private val mFill = Paint()
    private val mStroke = Paint()
    private val mParser = OpenGraphParser()
    private val mOGCache: OpenGraphCache = OpenGraphCache
    private val mTaskManager: DefaultTaskManager = DefaultTaskManager

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        mRootContainer = inflate(context, R.layout.view_open_graph, this)
        val array =
            context.obtainStyledAttributes(attrs, R.styleable.OpenGraphView, defStyleAttr, 0)
        if (attrs == null) {
            return
        }
        mSeparator = mRootContainer.findViewById(R.id.separator)
        setWillNotDraw(false)
        mFill.style = Paint.Style.FILL
        mFill.isAntiAlias = true
        mFill.color = ContextCompat.getColor(context, R.color.colorWhite)

        mStroke.style = Paint.Style.STROKE
        mStroke.isAntiAlias = true
        mStroke.color = ContextCompat.getColor(context, R.color.light_gray)

        mRoundableImageView = mRootContainer.findViewById<View>(R.id.og_image) as RoundableImageView
        mFavicon = mRootContainer.findViewById<View>(R.id.favicon) as ImageView
        setBgColor(
            array.getColor(
                R.styleable.OpenGraphView_bgColor,
                ContextCompat.getColor(context, R.color.colorWhite)
            )
        )
        (mRootContainer.findViewById<View>(R.id.og_title) as TextView).setTextColor(
            array.getColor(
                R.styleable.OpenGraphView_titleColor,
                ContextCompat.getColor(context, R.color.text_black)
            )
        )
        (mRootContainer.findViewById<View>(R.id.og_description) as TextView).setTextColor(
            array.getColor(
                R.styleable.OpenGraphView_descTextColor,
                ContextCompat.getColor(context, R.color.text_black)
            )
        )
        (mRootContainer.findViewById<View>(R.id.og_url) as TextView).setTextColor(
            array.getColor(
                R.styleable.OpenGraphView_urlTextColor,
                ContextCompat.getColor(context, R.color.base_gray)
            )
        )
        setStrokeColor(
            array.getColor(
                R.styleable.OpenGraphView_strokeColor,
                ContextCompat.getColor(context, R.color.light_gray)
            )
        )
        setStrokeWidth()
        setCornerRadius(array.getDimension(R.styleable.OpenGraphView_cornerRadius, 0f))
        val attrPosition = array.getInteger(R.styleable.OpenGraphView_imagePosition, 0)

        setImagePosition(if (attrPosition == 0 || attrPosition != 1) IMAGE_POSITION.LEFT else IMAGE_POSITION.RIGHT)
        mSeparate = array.getBoolean(R.styleable.OpenGraphView_separateImage, true)
        mSeparator.visibility = if (mSeparate) View.VISIBLE else View.GONE
        mRoundableImageView.setBackgroundColor(
            array.getColor(
                R.styleable.OpenGraphView_imagePlaceHolder,
                ContextCompat.getColor(getContext(), R.color.light_gray)
            )
        )
        mRootContainer.findViewById<View>(R.id.favicon).setBackgroundColor(
            array.getColor(
                R.styleable.OpenGraphView_faviconPlaceHolder,
                ContextCompat.getColor(getContext(), R.color.light_gray)
            )
        )
        array.recycle()
    }

    private fun setBgColor(@ColorInt bgColor: Int) {
        mBgColor = bgColor
        mFill.color = mBgColor
    }

    private fun setStrokeColor(@ColorInt strokeColor: Int) {
        mStrokeColor = strokeColor
        mStroke.color = strokeColor
    }

    private fun setStrokeWidth() {
        mStrokeWidth =
            context.resources.getDimensionPixelOffset(R.dimen.default_stroke_width)
                .toFloat()
        val param =
            mSeparator.layoutParams as LayoutParams
        param.width = mStrokeWidth.toInt()
        mSeparator.layoutParams = param
        mSeparator.setBackgroundColor(mStrokeColor)
        mFillRect[mStrokeWidth, mStrokeWidth, mViewWidth - mStrokeWidth] =
            mViewHeight - mStrokeWidth
        mStroke.strokeWidth = mStrokeWidth
        val defaultImageSize =
            context.resources.getDimensionPixelOffset(R.dimen.default_image_size)
        mRoundableImageView.setMargin(defaultImageSize, mStrokeWidth.toInt())
        invalidate()
    }

    private fun setCornerRadius(cornerRadius: Float) {
        var cornerRadius = cornerRadius
        if (cornerRadius < 0) {
            cornerRadius = 0f
        }
        mCornerRadius = cornerRadius
        mRoundableImageView.setRadius(cornerRadius)
        invalidate()
    }

    private fun setImagePosition(position: IMAGE_POSITION) {
        val parentPadding =
            context.resources.getDimensionPixelOffset(R.dimen.default_stroke_width)
        val parent = mRootContainer.findViewById<View>(R.id.parent)
        if (position == IMAGE_POSITION.LEFT) {
            setContentParam(RIGHT_OF)
            setSeparatorParam(RIGHT_OF)
            parent.setPadding(parentPadding, 0, 0, 0)
        } else if (position == IMAGE_POSITION.RIGHT) {
            setContentParam(LEFT_OF)
            setSeparatorParam(LEFT_OF)
            parent.setPadding(0, 0, parentPadding, 0)
        }
        mRoundableImageView.setPosition(position)
    }

    private fun setContentParam(rule: Int) {
        val contentsParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        )
        contentsParams.addRule(rule, mRoundableImageView.id)
        val contents = mRootContainer.findViewById<View>(R.id.og_contents)
        contents.layoutParams = contentsParams
        val padding =
            context.resources.getDimensionPixelOffset(R.dimen.default_content_padding)
        contents.setPadding(padding, padding, padding, padding)
    }

    private fun setSeparatorParam(rule: Int) {
        val separatorParams = LayoutParams(
            mStrokeWidth.toInt(), ViewGroup.LayoutParams.MATCH_PARENT
        )
        separatorParams.addRule(rule, mRoundableImageView.id)
        mSeparator.layoutParams = separatorParams
    }

    fun setOnLoadListener(listener: OnLoadListener) {
        mOnLoadListener = listener
    }

    fun loadFrom(url: String) {
        if (url.isEmpty() || !URLUtil.isNetworkUrl(
                url
            ) || url == "http://" || url == "https://"
        ) {
            visibility = View.GONE
            return
        } else {
            visibility = View.VISIBLE
        }
        setImage(null)
        mUri = Uri.parse(url)
        mUrl = url
        mSeparator.visibility = View.GONE
        val ogData: OGData? = mOGCache.get(url)
        if (ogData == null) {
            mOnLoadListener?.onLoadStart()
            mTaskManager.execute(
                OpenGraphDataTask(
                    OpenGraphDataCallable(url, mParser),
                    object : BaseTask.OnLoadListener<OGData> {
                        override fun onLoadSuccess(t: OGData) {
                            mOGCache.add(url, t)
                            t.image?.let { loadImage(it) }
                            loadFavicon(url)
                            setOpenGraphData(t)
                            mOnLoadListener?.onLoadFinish()
                        }

                        override fun onLoadError(e: Throwable?) {
                            mOnLoadListener?.onLoadError(e)
                        }
                    })
            )
        } else {
            ogData.image?.let { loadImage(it) }
            loadFavicon(url)
            setOpenGraphData(ogData)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.apply {
            drawRoundRect(mStrokeRect, mCornerRadius, mCornerRadius, mStroke)
            drawRoundRect(mFillRect, mCornerRadius, mCornerRadius, mFill)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mViewWidth = w
        mViewHeight = h
        if (mStrokeRect.width() == 0f && mStrokeRect.height() == 0f) {
            val padding = mStrokeWidth / 2
            mStrokeRect[padding, padding, mViewWidth - padding] = mViewHeight - padding
        }
    }

    private fun loadImage(url: String) {
        val bitmap = mOGCache.getImage(url)
        if (bitmap == null) {
            mTaskManager.execute(
                ImageTask(
                    ImageCallable(url),
                    object : BaseTask.OnLoadListener<Bitmap> {

                        override fun onLoadSuccess(t: Bitmap) {
                            mOGCache.addImage(url, t)
                            setImage(t)
                        }

                        override fun onLoadError(e: Throwable?) {
                            setImage(null)
                        }
                    })
            )
        } else {
            setImage(bitmap)
        }
    }

    private fun setImage(bitmap: Bitmap?) {
        mRoundableImageView.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.transparent
            )
        )
        mRoundableImageView.visibility = if (bitmap == null) View.GONE else View.VISIBLE
        mSeparator.visibility = if (bitmap == null) View.GONE else View.VISIBLE
        mRoundableImageView.setImageBitmap(bitmap)
        if (mSeparate) {
            mSeparator.visibility = View.VISIBLE
        }
    }

    private fun loadFavicon(url: String) {
        val uri = Uri.parse(url)
        val host = uri.host?.let {
            if (it.startsWith("www.")) it.substring(4) else it
        }
        host?.let {
            val favicon = mOGCache.getImage(it)
            if (favicon == null) {
                mTaskManager.execute(
                    FaviconTask(
                        FaviconCallable(it),
                        object : BaseTask.OnLoadListener<Bitmap> {
                            override fun onLoadSuccess(t: Bitmap) {
                                mOGCache.addImage(it, t)
                                setFavicon(t)
                            }

                            override fun onLoadError(e: Throwable?) {
                                setFavicon(null)
                            }
                        })
                )
            } else {
                setFavicon(favicon)
            }
        }
    }

    private fun setFavicon(bitmap: Bitmap?) {
        mFavicon.visibility = if (bitmap == null) View.GONE else View.VISIBLE
        mFavicon.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.transparent
            )
        )
        mFavicon.setImageBitmap(bitmap)
    }

    private fun setOpenGraphData(data: OGData?) {
        val url = mRootContainer.findViewById<View>(R.id.og_url) as TextView
        val title = mRootContainer.findViewById<View>(R.id.og_title) as TextView
        val description = mRootContainer.findViewById<View>(R.id.og_description) as TextView
        if (data == null) {
            title.text = mUrl
            url.text = ""
            description.text = ""
            mRoundableImageView.visibility = View.GONE
            return
        }
        val isImageEmpty = data.image.isNullOrEmpty()
        mRoundableImageView.visibility = if (isImageEmpty) View.GONE else View.VISIBLE
        if (data.title.isNullOrEmpty() && data.description.isNullOrEmpty()) {
            title.text = mUrl
            description.text = ""
        } else {
            data.title?.let {
                title.text = cleanSource(it)
            }
            data.description?.let {
                description.text = cleanSource(it)
            }
        }
        val uri = Uri.parse(mUrl)
        val host = uri.host
        url.text = host ?: mUrl
    }

    fun getUrl(): String? {
        return mUrl
    }

    fun clear() {
        mRoundableImageView.setImageResource(0)
        (mRootContainer.findViewById<View>(R.id.og_url) as TextView).text = ""
        (mRootContainer.findViewById<View>(R.id.og_title) as TextView).text = ""
        (mRootContainer.findViewById<View>(R.id.og_description) as TextView).text = ""
        (mRootContainer.findViewById<View>(R.id.favicon) as ImageView).setImageResource(
            0
        )
    }

    private fun cleanSource(source: String): Spanned {
        return HtmlCompat.fromHtml(source, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}