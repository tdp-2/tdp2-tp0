package com.tdp2.utils.ui.filter

import android.text.InputFilter
import android.text.Spanned

@Suppress("unused")
class NumericInputFilter : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        return if (source.toString().toIntOrNull() == null)
            ""
        else source
    }
}