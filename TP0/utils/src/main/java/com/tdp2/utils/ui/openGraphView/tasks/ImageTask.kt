package com.tdp2.utils.ui.openGraphView.tasks

import android.graphics.Bitmap
import java.io.IOException
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException

class ImageTask(callable: Callable<Bitmap>, listener: OnLoadListener<Bitmap>) :
    BaseTask<Bitmap>(callable, listener) {

    override fun done() {
        super.done()

        if (isCancelled) {
            return
        }
        val bm = try {
            get()
        } catch (e: InterruptedException) {
            onError(e)
            return
        } catch (e: ExecutionException) {
            onError(e)
            return
        }
        if (bm == null) {
            onError(IOException("Image is null."))
            return
        }
        onSuccess(bm)
    }
}