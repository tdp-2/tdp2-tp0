package com.tdp2.utils.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.tdp2.utils.R
import com.tdp2.utils.general.Event
import com.google.android.material.snackbar.Snackbar


fun View.makeSnackbar(
    @ColorInt backgroundColor: Int,
    title: String,
    @ColorInt titleColor: Int,
    timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    val snackbarText = SpannableStringBuilder()

    snackbarText.append(title)
    snackbarText.setSpan(
        ForegroundColorSpan(titleColor),
        0,
        snackbarText.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    val snackbar = Snackbar.make(this, snackbarText, timeLength)
    snackbar.view.setBackgroundColor(backgroundColor)
    snackbar.setActionTextColor(context.themeAccentColor())

    snackbar.view.layoutParams = when (val params = snackbar.view.layoutParams) {
        is CoordinatorLayout.LayoutParams -> {
            params.gravity = Gravity.TOP
            params.topMargin += extraTopMargin
            params
        }
        is FrameLayout.LayoutParams -> {
            params.gravity = Gravity.TOP
            params.topMargin += extraTopMargin
            params
        }
        else -> {
            params
        }
    }

    return snackbar
}

@Suppress("unused")
fun View.makeCustomSnackbar(
    title: String,
    buttomTitle: String? = null,
    onClickListener: View.OnClickListener? = null,
    timeLength: Int = Snackbar.LENGTH_LONG
): Snackbar {

    val snackbar = Snackbar.make(this, "", timeLength)
    snackbar.view.layoutParams = when (val params = snackbar.view.layoutParams) {
        is CoordinatorLayout.LayoutParams -> {
            params.gravity = Gravity.TOP
            params
        }
        is FrameLayout.LayoutParams -> {
            params.gravity = Gravity.TOP
            params
        }
        else -> {
            params
        }
    }

    snackbar.view.setBackgroundColor(context.themePrimaryColor())
    val snackbarView = snackbar.view as Snackbar.SnackbarLayout

    // Get custom view from external layout xml file.
    val inflater = LayoutInflater.from(this.context)
    val customView = inflater.inflate(R.layout.view_snackbar, null)
    val textView = customView.findViewById<TextView>(R.id.title)
    if (buttomTitle != null) {
        val button = customView.findViewById<TextView>(R.id.button)
        button.visibility = View.VISIBLE
        button.text = buttomTitle
        button.setOnClickListener(onClickListener)
    }

    textView.text = title

    snackbarView.addView(customView, 0)
    return snackbar
}

@Suppress("unused")
fun View.makeErrorSnackbar(
    @StringRes title: Int, timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeErrorSnackbar(this.context.getString(title), timeLength, extraTopMargin)
}

fun View.makeErrorSnackbar(
    title: String,
    timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeSnackbar(
        ContextCompat.getColor(this.context, R.color.colorRed),
        title,
        ContextCompat.getColor(this.context, R.color.colorWhite),
        timeLength,
        extraTopMargin
    )
}

@Suppress("unused")
fun View.makeSucessfulSnackbar(
    @StringRes title: Int, timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeSucessfulSnackbar(this.context.getString(title), timeLength, extraTopMargin)
}

fun View.makeSucessfulSnackbar(
    title: String,
    timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeSnackbar(
        ContextCompat.getColor(this.context, R.color.colorGreen),
        title,
        ContextCompat.getColor(this.context, R.color.colorWhite),
        timeLength,
        extraTopMargin
    )
}

fun View.makeSnackbar(
    @StringRes title: Int, timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeSnackbar(this.context.getString(title), timeLength, extraTopMargin)
}

fun View.makeSnackbar(
    title: String,
    timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
): Snackbar {
    return makeSnackbar(
        context.themePrimaryColor(),
        title,
        ContextCompat.getColor(this.context, R.color.colorWhite),
        timeLength,
        extraTopMargin
    )
}

fun View.showSnackbar(
    snackbarText: String,
    timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
) {
    this.makeSnackbar(snackbarText, timeLength, extraTopMargin).show()
}

fun View.showSnackbar(
    @StringRes message: Int, timeLength: Int = Snackbar.LENGTH_LONG,
    extraTopMargin: Int = 0
) {
    this.makeSnackbar(message, timeLength, extraTopMargin).show()
}

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
@Suppress("unused")
fun View.setupSnackbarRes(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int,
    extraTopMargin: Int = 0
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbar(context.getString(it), timeLength, extraTopMargin)
        }
    })
}

@Suppress("unused")
fun View.setupSnackbar(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<String>>,
    timeLength: Int,
    extraTopMargin: Int = 0
) {

    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showSnackbar(it, timeLength, extraTopMargin)
        }
    })
}

// TOAST
fun View.showToast(text: String, timeLength: Int) {
    Toast.makeText(context, text, timeLength).show()
}

@Suppress("unused")
fun View.setupToast(
    lifecycleOwner: LifecycleOwner,
    toastEvent: LiveData<Event<Int>>,
    timeLength: Int
) {

    toastEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let {
            showToast(context.getString(it), timeLength)
        }
    })
}

// ANIMATE VISIBILITY
@Suppress("unused")
fun View.visible(animate: Boolean = true) {
    if (animate) {
        animate().alpha(1f).setDuration(300).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                visibility = View.VISIBLE
            }
        })
    } else {
        visibility = View.VISIBLE
    }
}

/** Set the View visibility to INVISIBLE and eventually animate view alpha till 0% */
@Suppress("unused")
fun View.invisible(animate: Boolean = true) {
    hide(View.INVISIBLE, animate)
}

/** Set the View visibility to GONE and eventually animate view alpha till 0% */
@Suppress("unused")
fun View.gone(animate: Boolean = true) {
    hide(View.GONE, animate)
}

private fun View.hide(hidingStrategy: Int, animate: Boolean = true) {
    if (animate) {
        animate().alpha(0f).setDuration(300).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                visibility = hidingStrategy
            }
        })
    } else {
        visibility = hidingStrategy
    }
}
