package com.tdp2.tp0.data.repository

import android.content.Context
import com.tdp2.tp0.data.local.AppPreferences
import com.tdp2.tp0.data.remote.ApiClient
import com.tdp2.tp0.data.remote.model.dto.UserDto
import com.tdp2.tp0.data.remote.utils.NetworkHandler
import com.tdp2.tp0.presentation.di.base.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val networkHandler: NetworkHandler,
    private val services: ApiClient,
    @ApplicationContext val context: Context
) {

    private val defaultScope = CoroutineScope(Dispatchers.Default)

    var user: UserDto? = AppPreferences.user
        private set

    val isLoggedIn: Boolean
        get() = user != null

    private fun updateSession(user: UserDto) {
        AppPreferences.updateSession(user)
        this.user = user
    }

    fun clearSession() {
        AppPreferences.clearSession()
        this.user = null
    }
}