package com.tdp2.tp0.data.local.model

data class User (

    val id : Int,
    val firstName : String?,
    val lastName : String?,
    val email : String
)