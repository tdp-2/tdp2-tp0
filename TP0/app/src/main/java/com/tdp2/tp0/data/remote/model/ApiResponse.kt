package com.tdp2.tp0.data.remote.model

abstract class ApiResponse {
    abstract val status: String
    abstract val statusCode: String
    abstract val statusMessage: String

    fun isSuccessful(): Boolean {
        if (this.status == "successful") {
            return true
        }
        return false
    }
}