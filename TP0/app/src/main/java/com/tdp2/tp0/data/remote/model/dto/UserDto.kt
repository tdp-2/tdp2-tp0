package com.tdp2.tp0.data.remote.model.dto

import java.io.Serializable

data class UserDto (

    val id : Int,
    val firstName : String?,
    val lastName : String?,
    val email : String
) : Serializable