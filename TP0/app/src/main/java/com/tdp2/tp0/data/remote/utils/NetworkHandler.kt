package com.tdp2.tp0.data.remote.utils

import android.content.Context
import com.tdp2.tp0.presentation.di.base.ApplicationContext
import com.tdp2.utils.extensions.networkState
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHandler @Inject constructor(@ApplicationContext private val context: Context) {
    val isConnected get() = context.networkState?.isConnected
}